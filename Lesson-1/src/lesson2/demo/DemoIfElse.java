package lesson2.demo;

public class DemoIfElse {
    public static void main(String[] args) {
        int x = 20;
        int y = 10;

//        if (x > y) {
//            System.out.println("X là số lớn hơn");
//        } else if (y > x) {
//            System.out.println("Y là số lớn hơn");
//        } else if (x == y) {
//            System.out.println("X bằng Y");
//        } else {
//            System.out.println("Các trường hợp còn lại");
//        }
        if (x> y){
            System.out.println();
        }

        String text = "";
        text = (x > 50) ? "Trường hợp đúng" : "Trường hợp sai";
        System.out.println(x > 50);
    }
}

package lesson2.demo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ABC {
    public static void main(String[] args) throws ParseException {
        // Định dạng ngày tháng năm theo patten
        Date date = new Date();
        System.out.println(date);
        // Tạo định dạng ngày tháng để in ra màn hình
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String stringDate = dateFormat.format(date);
        System.out.println(stringDate);

        // Định dạng ngày tháng năm theo khu vực
        Locale locale = new Locale("vi", "VN");
        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String stringDate2 = dateFormat1.format(date);
        System.out.println(stringDate2);

        // Chuyển đổi từ chuỗi -> kiểu dữ liệu Date
        String dateString = "24-02-2022 20:15:30";
        DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date3 = dateFormat2.parse(dateString);
        System.out.println(date3);

    }
}

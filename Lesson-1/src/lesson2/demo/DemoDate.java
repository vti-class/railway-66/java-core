package lesson2.demo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class DemoDate {
    public static void main(String[] args) {
        //		Date Format By Country
        Locale locale = new Locale("vi", "VN");
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String date = dateFormat.format(new Date());
//        System.out.print(date);



//		Date Format by pattern
        Date date2 = new Date();
        System.out.println(date2.getYear());
        LocalDate localDate = LocalDate.of(2014, 9, 11);

        System.out.println("Giá trị date nguyên bản: " + date);
        System.out.println("Gi trị localDate: " + localDate);

        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String stringDate = dateFormat2.format(date2);
        System.out.println("Giá trị date sau khi biến thành format theo định dang: " + stringDate);
    }
}

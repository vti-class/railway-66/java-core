package lesson2.demo;

public class DemoSwitchCase {
    public static void main(String[] args) {
        int number = 6;

        switch (number){
            case 1: // các giá trị để so sánh với biến truyền vào là number
                System.out.println("Trường hợp 1");
                break;
            case 2:
                System.out.println("Trường hợp 2");
                break;
            case 3:
                System.out.println("Trường hợp thứ 3");
                break;
            default:
                System.out.println("Các trường hợp còn lại");
                break;
        }
    }
}

package lesson2.assignment2;

import lesson1.Assignment1.Account;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class Assignment2 {
    public static void main(String[] args) {
//        --------------------------- Exercise 4: Random Number ---------------------------
        System.out.println("Exercise 4: Random Number");
        System.out.println("---- question 1 -----");
        Random random = new Random();
        int number = random.nextInt();
        System.out.println("Số nguyên bất kỳ là: " + number);

        System.out.println("---- question 2 -----");
       float number2 = random.nextFloat();
        System.out.println("Số thực bất kỳ là: " + number2);

        System.out.println("---- question 3 -----");
        String[] arrName = {"Nguyễn Văn A", "Nguyễn Văn B", "Nguyễn Văn C"};
        int numberRandom = random.nextInt(2);
        String name = arrName[numberRandom];
        System.out.println(name);

        System.out.println("---- question 4 -----");
        int minDate = (int) LocalDate.of(1995, 7, 24).toEpochDay();
        int maxDate = (int) LocalDate.of(1995, 12, 20).toEpochDay();
        long randomLong = minDate + random.nextInt(maxDate - minDate);
        LocalDate localDate = LocalDate.ofEpochDay(randomLong);
        System.out.println(localDate);

//        --------------------------- Exercise 5: Input from console ---------------------------
        System.out.println("Exercise 5: Input from console");
        System.out.println("---- question 1 -----");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào số nguyên thứ nhất:");
        int number1 = scanner.nextInt();
        System.out.println("Mời bạn nhập vào số nguyên thứ hai:");
        int num2 = scanner.nextInt();

        System.out.println("Các số vừa nhập vào là: " + number1 + ", " + num2);




        System.out.println("---- question 5 -----");
        Account account = new Account();





        // Ví dụ để nhập vào 1 số đến khi đúng thì thôi, fix được lỗi chạy con trỏ
//        int a = 0;
//        System.out.println("Mời bạn nhập vào 1 số");
//        while (true){
//            try {
//                a = Integer.parseInt(scanner.nextLine());
//                System.out.println("Số vừa nhập vào là: "+ a);
//                break;
//            } catch (Exception exception){
//                System.out.println("Bạn vừa nhập sai, mời bạn nhập lại");
//            }
//        }





    }
}

package lesson1.Assignment1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Program {
    public static void main(String[] args) {
        System.out.println("Khởi tạo giá trị cho 1 đối tượng Account");

//  -------------------- Khởi tạo giá trị cho đối tương DEPARTMENT --------------------
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "PHòng ban 1";
        System.out.println(department1.departmentName);

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "PHòng ban 2";
        System.out.println(department2.departmentName);

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "PHòng ban 3";

//  -------------------- Khởi tạo giá trị cho đối tương GROUP --------------------
        Group group1 = new Group();

        Group group2 = new Group();

        Group group3 = new Group();


//  -------------------- Khởi tạo giá trị cho danh sách Group --------------------

        Group[] arrGr1 = {group1, group2, group3};

//  -------------------- Khởi tạo giá trị cho đối tương POSITION --------------------
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.DEV;

        Position position2 = new Position();
        position1.positionId = 2;
        position1.positionName = PositionName.TEST;

        Position position3 = new Position();
        position3.positionId = 2;
        position3.positionName = PositionName.SCRUMMASTER;

//  -------------------- Khởi tạo giá trị cho đối tươngh ACCOUNT --------------------
        Account account1 = new Account();
        account1.accountId = 1;
        account1.department = department1;
        account1.email = "abc1@gmail.com";
        account1.username = "Username 1";
        account1.fullName = "Nguyễn Văn A";
        account1.position = position1;
        account1.createDate = new Date();

        Account account2 = new Account();
        account2.groups = arrGr1;
        account2.accountId = 2;
        account2.department = department2;
        account2.email = "abc2@gmail.com";
        account2.username = "Username 2";
        account2.fullName = "Nguyễn Văn B";
        account2.position = position2;
        account2.createDate = new Date();

        Account account3 = new Account();
        account3.accountId = 3;
        account3.department = department3;
        account3.email = "abc3@gmail.com";
        account3.username = "Username 3";
        account3.fullName = "Nguyễn Văn C";
        account3.position = position3;
        account3.createDate = new Date();

//  -------------------- Khởi tạo giá trị cho danh sách account --------------------
        Account[] arrAccount1 = {account1, account3};

        group1.accounts = arrAccount1;


//  -------------------- Khởi tạo giá trị cho Exam --------------------
        Exam exam1 = new Exam();
        exam1.createDate = new Date();


//---------------------------------- Assignment 2 ----------------------------------
        System.out.println("---------- Question 1: -----------");
        if(account2.department == null){
            System.out.println("Nhân viên này chưa có phòng ban");
        } else {
            System.out.println("Phòng ban của nhân viên này là: " + account2.department.departmentName);
        }

        System.out.println("---------- Question 2: -----------");
        if (account2.groups.length < 3){
            System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
        } else if (account2.groups.length == 3) {
            System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
        } else if (account2.groups.length == 4) {
            System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
        }else {
            System.out.println("Các trường hợp còn lại");
        }

        System.out.println("---------- Question 5: -----------");
        int accountLength = group1.accounts.length;
        switch (accountLength){
            case 1:
                System.out.println("Nhóm có một thành viên");
                break;
            case 2:
                System.out.println("Nhóm có 2 thành viên");
                break;
            case 3:
                System.out.println("Nhóm có 3 thành viên");
                break;
            default:
                System.out.println("Nhóm có nhiều thành viên");
                break;
        }

        System.out.println("---------- Question 10: -----------");
        Account[] arrAccount2 = {account1, account2};
        // Cách 1: Dùng for each
        int stt = 1;
        for(Account account :  arrAccount2){
            System.out.println("----------------------");
            System.out.println("Thông tin của account thứ " + stt + " là:");
            System.out.println("Email: " + account.email);
            System.out.println("Full name: " + account.fullName);
            System.out.println("Phòng bàn: " + account.position.positionName);
            stt++;
        }

        // Cách 2: Dùng for i
        for(int i = 0; i < arrAccount2.length; i++){
            System.out.println("----------------------");
            System.out.println("Thông tin của account thứ " + (i +1) + " là:");
            System.out.println("Email: " + arrAccount2[i].email);
            System.out.println("Full name: " + arrAccount2[i].fullName);
            System.out.println("Phòng bàn: " + arrAccount2[i].position.positionName);
        }

        System.out.println("---------- Question 16: -----------");
        // Cách 1: Hàm while thường
        int number = 0;
        while(number < arrAccount2.length){
            System.out.println("----------------------");
            System.out.println("Thông tin của account thứ " + (number + 1) + " là:");
            System.out.println("Email: " + arrAccount2[number].email);
            System.out.println("Full name: " + arrAccount2[number].fullName);
            System.out.println("Phòng bàn: " + arrAccount2[number].position.positionName);

            number++;
        }

        // Cách 1: Dùng do while
        int number2 = 0;
        do {

            System.out.println("----------------------");
            System.out.println("Thông tin của account thứ " + (number2 + 1) + " là:");
            System.out.println("Email: " + arrAccount2[number2].email);
            System.out.println("Full name: " + arrAccount2[number2].fullName);
            System.out.println("Phòng bàn: " + arrAccount2[number2].position.positionName);
            number2++;

        } while (number2 <= (arrAccount2.length - 1));// arrAccount2.length - 1 = 1


        // Exercise 3: Date Format
        System.out.println("---------- Exercise 3: Date Format -----------");
        System.out.println("---------- Question 1: -----------");
        Locale locale = new Locale("vi", "VN");
        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String createDate = dateFormat1.format(exam1.createDate);
        System.out.println(createDate);


        System.out.println("---------- Question 2: -----------");
        DateFormat dateFormat = new SimpleDateFormat("yyyy - MM - dd - hh - mm - ss");
        String createDate2 = dateFormat.format(exam1.createDate);
        System.out.println(createDate2);
    }
}

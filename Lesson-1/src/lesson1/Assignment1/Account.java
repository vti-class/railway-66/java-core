package lesson1.Assignment1;

import java.util.Date;

public class Account {
    int accountId;
    String email;
    String username;
    String fullName;
    Department department;
    Position position;
    Date createDate;

    Group[] groups;

    public Account() {
    }

    public Account(int accountId, String email, String username, String fullName, Department department, Position position, Date createDate, Group[] groups) {
        this.accountId = accountId;
        this.email = email;
        this.username = username;
        this.fullName = fullName;
        this.department = department;
        this.position = position;
        this.createDate = createDate;
        this.groups = groups;
    }
}

package lesson1.demo.dataType;

public enum Gender {
    MALE, FEMALE, UNKNOWN;
}

package lesson1.demo.object;

public class Department {

	int departmentId;

	String departmentName;

	@Override
	public String toString() {
		return "lesson1.demo.object.Department{" +
				"departmentId=" + departmentId +
				", departmentName='" + departmentName + '\'' +
				'}';
	}

}

package lesson1.demo.object;

public class Position {

	int id;

	PositionName name;

	@Override
	public String toString() {
		return "lesson1.demo.object.Position{" +
				"id=" + id +
				", name=" + name +
				'}';
	}

}

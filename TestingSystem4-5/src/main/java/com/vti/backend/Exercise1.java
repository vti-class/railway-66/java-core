package com.vti.backend;

import com.vti.entity.Account;

public class Exercise1 {

    public void question1(){
        Account account = new Account();
        // Set giá trị cho accountID
//        account.accountId = 1;
        // Set giá trị cho username bằng phương thức setter
        account.setUsername("Nguyễn văn A");

        // Lấy giá trị của username bằng phương thức getter
        System.out.println(account.getUsername());


        question2();
    }

    private void question2(){
        Account account = new Account();
    }

    private void question3(){
        Account account = new Account();
    }
}

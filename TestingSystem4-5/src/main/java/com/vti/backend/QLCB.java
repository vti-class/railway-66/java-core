package com.vti.backend;

import com.vti.entity.CanBo;
import com.vti.entity.Gender;
import com.vti.utils.ScannerUtils;

import java.util.List;
import java.util.Scanner;

public class QLCB {

    public CanBo themMoiCB(){
        System.out.println("Mời nhập vào tên cán bộ");
        String fullName = ScannerUtils.inputString();
        System.out.println("Mời nhập vào tuổi cán bộ");
        int age = ScannerUtils.inputNumber();

        Gender gender;
        System.out.println("Mời bạn chọn giới tính của bán bộ: ");
        System.out.println("1. Nam");
        System.out.println("2. Nữ");
        System.out.println("3. Khác");
        int chose = ScannerUtils.inputNumber();
        switch (chose){
            case 1:
                gender = Gender.NAM;
                break;
            case 2:
                gender = Gender.NU;
                break;
            default:
                gender = Gender.KHAC;
                break;
        }
        System.out.println("Mời bạn nhập địa chỉ cán bộ:");
        String address = ScannerUtils.inputString();
        // Cách 1: Sử dụng contructor -> tạo đối tượng CanBo
        CanBo canBo = new CanBo(fullName, age, gender, address);

        // Cách 2: Sử dụng setter
//        CanBo canBo1 = new CanBo();
//        canBo1.setFullName(fullName);
//        canBo1.setAge(age);
        System.out.println(canBo);

        return canBo;
    }

    public CanBo findCanBo(List<CanBo> canBoList){
        System.out.println("NHập vào họ tên mà bạn tìm kiếm:");
        String fullName = ScannerUtils.inputString();
        CanBo canBo = new CanBo();
        for (CanBo cb: canBoList) {
            if (fullName.equals(cb.getFullName())){
                canBo = cb;
                break;
            }
        }
        return canBo;
    }
}

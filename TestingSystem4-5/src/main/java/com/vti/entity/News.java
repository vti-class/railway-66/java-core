package com.vti.entity;

import com.vti.backend.INews;
import com.vti.utils.ScannerUtils;

public class News implements INews {
    private int id;

    private String title;

    private String publishDate;

    private String author;

    private String content;

    private float averageRate;

    @Override
    public void display() {
        System.out.println(title);
        System.out.println(this.publishDate);
        System.out.println(this.content);
        System.out.println(this.author);
        System.out.println(this.averageRate);
    }

    @Override
    public float calculate() {
        System.out.println("Mời bạn đánh giá");
        int[] rates = new int[3];
        rates[0] = ScannerUtils.inputNumber(1,5);
        rates[1] = ScannerUtils.inputNumber(1,5);
        rates[2] = ScannerUtils.inputNumber(1,5);
        this.averageRate = (rates[0] + rates[1] + rates[2])/3.0f;
        return averageRate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public float getAverageRate() {
        return averageRate;
    }


    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", publishDate='" + publishDate + '\'' +
                ", author='" + author + '\'' +
                ", content='" + content + '\'' +
                ", averageRate=" + averageRate +
                '}';
    }

}

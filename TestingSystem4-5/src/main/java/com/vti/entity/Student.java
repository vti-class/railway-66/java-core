package com.vti.entity;

public class Student extends Person{
    private String msv;
    private String clazz;

    public Student() {
        this.msv = "123";
        this.clazz = "Ra66";
        this.age = 2;
        this.name = "Nguyễn Văn A";
    }

    @Override
    public String toString() {
        return "Student{" +
                "msv='" + msv + '\'' +
                ", clazz='" + clazz + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

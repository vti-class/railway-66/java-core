package com.vti.entity;

public class CongNhan extends CanBo{
    private int bac;

    public CongNhan(String fullName, int age) {
        super(fullName, age);
        System.out.println("Hàm khởi tạo với biến truyền vào là tên và tuổi");
    }

    public CongNhan() {
        System.out.println("Hàm khởi tạo ko tham số");
    }

    public CongNhan(int bac) {
        this();
        this.bac = bac;
        System.out.println("Hàm khởi tạo với biến truyền vào là cấp bậc");
    }
}

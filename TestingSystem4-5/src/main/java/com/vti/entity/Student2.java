package com.vti.entity;

import java.util.Scanner;

public class Student2 {
    private int id;
    private String name;
    private String hometown;
    private float grading;

    public Student2() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập vào tên: ");
        String name = scanner.nextLine();
        this.name = name;
        System.out.println("Nhập vào địa chỉ: ");
        String hometown = scanner.nextLine();
        this.hometown = hometown;
        this.grading = 0;
    }

    public void setGrading(float grading) {
        this.grading = grading;
    }

    public void addGrading(float grading){
        this.grading += grading;
    }

    @Override
    public String toString() {
        String type = "";
        if (this.grading < 4){
            type = "Yếu";
        } else if (this.grading >= 4 && this.grading <6){
            type = "Trung bình";
        } else if (this.grading >= 6 && this.grading < 8){
            type = "Khá";
        } else {
            type = "Giỏi";
        }
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hometown='" + hometown + '\'' +
                ", grading=" + type +
                '}';
    }
}

package com.vti.frontend;

import com.vti.backend.QLCB;
import com.vti.entity.CanBo;
import com.vti.entity.CongNhan;
import com.vti.entity.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<CanBo> canBoList = new ArrayList<>();
        QLCB qlcb = new QLCB();
        while (true){
            System.out.println("----".repeat(20));
            System.out.println("Mời bạn chọn chức năng: ");
            System.out.println("1. Thêm mới cán bộ.");
            System.out.println("2. Tìm kiếm theo họ tên.");
            System.out.println("3. Hiện thị thông tin về danh sách các cán bộ.");
            System.out.println("4. Nhập vào tên của cán bộ và delete cán bộ đó");
            System.out.println("5. Thoát khỏi chương trình");
            int chose = Integer.parseInt(scanner.nextLine());

            switch (chose){
                case 1:
                    CanBo canBo = qlcb.themMoiCB();
                    canBoList.add(canBo);
                    break;
                case 2:
                    CanBo canBo2 = qlcb.findCanBo(canBoList);
                    System.out.println(canBo2);
                    break;
                case 3:
                    for (CanBo cb : canBoList) {
                        System.out.println(cb);
                    }
                    break;
                case 4:
                    CanBo canBo4 = qlcb.findCanBo(canBoList);
                    canBoList.remove(canBo4); // Truyền đối tượng cán bộ muốn xoá
                    System.out.println("Đã xoá thành công!");
                    break;
                case 5:
                    return;
            }
        }
    }
}

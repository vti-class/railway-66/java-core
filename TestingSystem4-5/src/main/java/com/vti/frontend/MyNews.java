package com.vti.frontend;

import com.vti.entity.CanBo;
import com.vti.entity.News;
import com.vti.utils.ScannerUtils;

public class MyNews {
    public static void main(String[] args) {
        News news = new News();
        while (true){
            System.out.println("----".repeat(20));
            System.out.println("Mời bạn chọn chức năng: ");
            System.out.println("1. Thêm mới một news.");
            System.out.println("2. Hiện thị thông tin news.");
            System.out.println("3. Đánh giá");
            System.out.println("4. Thoát khỏi chương trình");
            int chose = ScannerUtils.inputNumber(1,4);
            switch (chose){
                case 1:
                    System.out.println("Mời bạn nhập vào tiêu đề:");
                    String title = ScannerUtils.inputString();
                    news.setTitle(title);

                    System.out.println("Mời bạn bạn nhập vào ngày công bố: ");
                    String publishDate = ScannerUtils.inputString();
                    news.setPublishDate(publishDate);
                    // Các giá trị Author, Content tương tự
                    break;
                case 2:
                    news.display();
                    break;
                case 3:
                    news.calculate();
                    news.display();
                    break;
                case 4:
                    return;
            }
        }
    }
}


class Test
{
    Test(Integer I)
    {
        System.out.print(0);
    }
    Test(int... i)
    {
        System.out.print(1);
    }
//    Test(double d)
//    {
//        System.out.print(2);
//    }
    Test(Object o)
    {
        System.out.print(3);
    }
    public static void main(String[] args)
    {
        new Test('a');
        new Test(10);
        new Test(10.5);
    }
}
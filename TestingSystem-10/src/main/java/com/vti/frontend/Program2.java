package com.vti.frontend;

import com.vti.utils.ScannerUtils;

public class Program2 {
    public static void main(String[] args) {
        FunctionDepartment function = new FunctionDepartment();
        while (true){
            System.out.println("----".repeat(20));
            System.out.println("Mời bạn chọn chức năng: ");
            System.out.println("1. Thêm mới department");
            System.out.println("2. Sửa một department");
            System.out.println("3. Xoá department theo id");
            System.out.println("4. Tìm kiếm department theo tên");
            System.out.println("5. Tất cả department");
            System.out.println("6. Thoát chương trình");
            int chose = ScannerUtils.inputNumber(1,6);
            switch (chose){
                case 1:
                    function.createDepartment();
                    break;
                case 2:
//                    function.updateAccount();
                    break;
                case 3:
//                    function.deleteAccount();
                    break;
                case 4:
//                    function.findByEmail();
                    break;
                case 5:
                    function.getDepartments();
                    break;
                case 6:
                    return;
            }
        }
    }
}

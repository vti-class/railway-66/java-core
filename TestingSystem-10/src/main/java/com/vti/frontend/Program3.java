package com.vti.frontend;

import com.vti.backend.controller.AccountController;
import com.vti.entity.Role;
import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

public class Program3 {
    static FunctionNew functionNew = new FunctionNew();
//    public static void main(String[] args) {
//        System.out.println("----------------------- Đăng nhập -----------------------");
//        while (true) {
//            User user = functionNew.login();
//            if (user == null) {
//                System.err.println("Username hoặc password không đúng, mời nhập lại!");
//            } else if (user.getRole() == Role.ADMIN) {
//                menuAdmin();
//            } else{
//                menuUser();
//            }
//        }
//    }

    public static void main(String[] args) {
        menuAdmin();
    }

    public static void menuUser(){
        Function function = new Function();
        while (true){
            System.out.println("------- Menu User ---------");
            System.out.println("Mời bạn chọn chức năng: ");
            System.out.println("1. Danh sách User");
            System.out.println("2. Tìm kiếm User theo Id");
            System.out.println("3. Tìm kiếm user");
            System.out.println("4. Tìm kiếm Account theo email");
            System.out.println("5. Tất cả account");
            System.out.println("6. Login vào hệ thống");
            System.out.println("7. Log out");
            System.out.println("8. Thoát chương trình");
            int chose = ScannerUtils.inputNumber(1,8);
            switch (chose){
                case 1:
                    functionNew.getAllUser();
                    break;
                case 2:
                    functionNew.findUserById();
                    break;
                case 3:
                    functionNew.findUser();
                    break;
                case 4:
                    function.findByEmail();
                    break;
                case 5:
                    function.getAllAccount();
                    break;
                case 6:
                    function.login();
                    break;
                case 7:
                    break;
                case 8:
                    return;
            }
            if (chose == 7){
                System.out.println("Bạn đã log out");
                break;
            }
        }
    }

    public static void menuAdmin(){
        Function function = new Function();
        while (true){
            System.out.println("------- Menu Admin ---------");
            System.out.println("1. Danh sách User");
            System.out.println("2. Xoá user theo id");
            System.out.println("3. Thay dổi password");
            System.out.println("4. Thêm mới user");
            System.out.println("5. Tìm kiếm Account theo email");
            System.out.println("6. Tất cả account");
            System.out.println("7. Login vào hệ thống");
            System.out.println("8. Thoát chương trình");
            int chose = ScannerUtils.inputNumber(1,7);
            switch (chose){
                case 1:
                    functionNew.getAllUser();
                    break;
                case 2:
                    functionNew.deleteUser();
                    break;
                case 3:
                    functionNew.updatePassword();
                    break;
                case 4:
                    functionNew.createUser();
                    break;
                case 5:
                    function.getAllAccount();
                    break;
                case 6:
                    function.login();
                    break;
                case 7:
                    return;
            }
        }
    }
}

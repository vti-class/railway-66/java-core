package com.vti.frontend;

import com.vti.backend.controller.AccountController;
import com.vti.entity.Account;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class Function {
    AccountController controller = new AccountController();

    public void createAccount(){
        System.out.println("Mời bạn nhập Username");
        String username = ScannerUtils.inputString();
        System.out.println("Mời bạn nhập vào email:");
        String email = ScannerUtils.inputEmail();
        System.out.println("Mời bạn nhập vào password");
        String password = ScannerUtils.inputPassword();
        controller.createAccount(username, email, password);
    }

    public void updateAccount(){
        System.out.println("Nhập vào id người dùng muốn thay đổi password:");
        int id = ScannerUtils.inputNumber();
        System.out.println(("Nhập vào password cũ:"));
        String oldPassword = ScannerUtils.inputPassword();
        System.out.println("Nhập vào password mới:");
        String newPassword = ScannerUtils.inputPassword();
        controller.updateAccount(id, oldPassword, newPassword);
    }

    public void deleteAccount(){
        System.out.println("Nhập vào id người dùng muốn xoá:");
        int id = ScannerUtils.inputNumber();
        controller.deleteAccount(id);
    }

    public void findByEmail(){
        System.out.println("Mời bạn nhập vào từ khóa muốn tìm kiếm: ");
        String key = ScannerUtils.inputString();
        List<Account> accountList = controller.findAllByEmail(key);
        String leftAlignFormat = "| %-3s| %-15s | %-17s | %-15s |%n";
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        System.out.format("| id |     fullName    |       email       |     password    |%n");
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        for (Account account: accountList) {
            System.out.format(leftAlignFormat, account.getAccountId(), account.getFullName(), account.getEmail(), account.getPassword());
        }
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
    }

    public void getAllAccount(){
        List<Account> accounts = controller.getAllAccount();
        String leftAlignFormat = "| %-3s| %-15s | %-17s | %-15s |%n";
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        System.out.format("| id |     fullName    |       email       |     password    |%n");
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
        for (Account account: accounts) {
            System.out.format(leftAlignFormat, account.getAccountId(), account.getFullName(), account.getEmail(), account.getPassword());
        }
        System.out.format("+----+-----------------+-------------------+-----------------+%n");
    }

    public void login(){
        System.out.println("Mời bạn nhập vào email: ");
        String email = ScannerUtils.inputEmail();
        System.out.println("Mời bạn nhập vào password: ");
        String password = ScannerUtils.inputPassword();
        if(controller.login(email, password)){
            System.out.println("Đăng nhập thành công!");
        } else {
            System.err.println("Đăng nhập thất bại");
        }
    }
}

package com.vti.frontend;

import com.vti.backend.controller.UserController;
import com.vti.entity.Account;
import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class FunctionNew {
    UserController userController = new UserController();
    public User login(){
        System.out.println("Mời bạn nhập username:");
        String username = ScannerUtils.inputString();
        System.out.println("Mời bạn nhập vào password:");
        String password = ScannerUtils.inputPassword();
        return userController.login(username, password);
    }

    public void getAllUser(){
        List<User> userList = userController.getAllUser();
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user: userList) {
            System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                    user.getDateOfBirth(), user.getDepartment().getDepartmentName());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }

    public void findUserById(){
        System.out.println("Mời bạn nhập vào id User muốn tìm kiếm");
        int id = ScannerUtils.inputNumber();
        User user = userController.findUserById(id);
        if (user != null){
            String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
            System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
                System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                        user.getDateOfBirth(), user.getDepartment().getDepartmentName());
            System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        } else {
            System.err.println("User Id không tồn tại");
        }
    }

    public void findUser(){
        System.out.println("Nhập từ khoá tìm kiếm: ");
        String key = ScannerUtils.inputString();
        List<User> userList = userController.findUser(key);

        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user: userList) {
            System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                    user.getDateOfBirth(), user.getDepartment().getDepartmentName());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }

    public void deleteUser(){
        System.out.println("Nhập vào User Id muốn xoá:");
        int id = ScannerUtils.inputNumber();
        User user = userController.findUserById(id);
        if (user == null){
            System.err.println("User Id không tồn tại");
        } else {
            userController.deleteUserById(id);
        }
    }

    public void updatePassword(){
        System.out.println("Nhập vào userId thay đổi password: ");
        int id = ScannerUtils.inputNumber();
        System.out.println("NHập vào password cũ: ");
        String oldPassword = ScannerUtils.inputPassword();
        System.out.println("Nhập vào password mới: ");
        String newPassword = ScannerUtils.inputPassword();
        userController.updateUser(id, oldPassword, newPassword);
    }

    public void createUser(){
        System.out.println("Nhập vào username: ");
        String username = ScannerUtils.inputString();
        System.out.println("Nhập vào email: ");
        String email = ScannerUtils.inputEmail();
        System.out.println("Nhập vào ngày sinh: ");
        String birthDay = ScannerUtils.inputString();

        System.out.println("Mời bạn bọn phòng ban: ");
        System.out.println("1. java");
        System.out.println("2. php");
        System.out.println("3. scrum master");
        int departmentId = ScannerUtils.inputNumber(1,3);
        userController.createUser(username, email, birthDay, departmentId);
    }

    public void createDepartment() {

    }
}

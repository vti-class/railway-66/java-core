package com.vti.frontend;

import com.vti.backend.controller.DepartmentController;
import com.vti.entity.Department;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class FunctionDepartment {
    DepartmentController departmentController = new DepartmentController();

    public void getDepartments(){
        List<Department> departmentList = departmentController.getDepartments();
        String leftAlignFormat = "| %-3s|   %-16s |%n";

        System.out.format("+----+--------------------+%n");
        System.out.format("| id |   department name  |%n");
        System.out.format("+----+--------------------+%n");
        for (Department department: departmentList) {
            System.out.format(leftAlignFormat, department.getDepartmentId(), department.getDepartmentName());
        }
        System.out.format("+----+--------------------+%n");
    }

    public void createDepartment(){
        System.out.println("Mời bạn nhập vào tên phòng ban");
        String departmentName = ScannerUtils.inputString();

        while (true){
            if (departmentController.isDepartmentNameExists(departmentName)){
                System.err.println("Tên phòng ban đã tồn tại, mời bạn nhập lại!");
                departmentName = ScannerUtils.inputString();
            } else {
                break;
            }
        }
        departmentController.createDepartment(departmentName);
        System.out.println("Thêm mới thành công");
    }
}

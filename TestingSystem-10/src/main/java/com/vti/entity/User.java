package com.vti.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class User {
    private int id;
    private Role role;
    private String username;
    private String password;
    private String email;
    private String dateOfBirth;
    private Department department;
}

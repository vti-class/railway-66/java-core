package com.vti.backend.controller;

import com.vti.backend.service.UserService;
import com.vti.entity.User;

import java.util.List;

public class UserController {
    UserService userService = new UserService();

    public User login(String username, String password) {
        return userService.login(username, password);
    }

    public List<User> getAllUser() {
        return userService.getAllUser();
    }

    public User findUserById(int id) {
        return userService.findUserById(id);
    }

    public List<User> findUser(String key) {
        return userService.findUser(key);
    }

    public void deleteUserById(int id) {
        userService.deleteUserById(id);
    }

    public void updateUser(int id, String oldPassword, String newPassword) {
        userService.updateUser(id, oldPassword, newPassword);
    }

    public void createUser(String username, String email, String birthDay, int departmentId) {
        userService.createUser(username, email, birthDay, departmentId);
    }
}

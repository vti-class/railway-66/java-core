package com.vti.backend.controller;

import com.vti.backend.service.DepartmentService;
import com.vti.entity.Department;

import java.util.List;

public class DepartmentController {
    DepartmentService departmentService = new DepartmentService();

    public List<Department> getDepartments() {
        return departmentService.getDepartments();
    }

    public boolean isDepartmentNameExists(String name) {
        return departmentService.isDepartmentNameExists(name);
    }

    public void createDepartment(String name) {
        departmentService.createDepartment(name);
    }
}

package com.vti.backend.service;

import com.vti.entity.User;

import java.util.List;

public interface IUserService {
    User login(String username, String password);

    List<User> getAllUser();

    User findUserById(int id);

    List<User> findUser(String key);

    void deleteUserById(int id);

    void updateUser(int id, String oldPassword, String newPassword);

    void createUser(String username, String email, String birthDay, int departmentId);
}

package com.vti.backend.service;

import com.vti.backend.repository.UserRepository;
import com.vti.entity.User;

import java.util.List;

public class UserService implements IUserService{
    UserRepository userRepository = new UserRepository();

    @Override
    public User login(String username, String password) {
        return userRepository.login(username, password);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.getAllUsers();
    }

    @Override
    public User findUserById(int id) {
        return userRepository.findUserById(id);
    }

    @Override
    public List<User> findUser(String key) {
        return userRepository.findUser(key);
    }

    @Override
    public void deleteUserById(int id) {
        userRepository.deleteUserById(id);
    }

    @Override
    public void updateUser(int id, String oldPassword, String newPassword) {
        userRepository.updateUser(id, oldPassword, newPassword);
    }

    @Override
    public void createUser(String username, String email, String birthDay, int departmentId) {
        userRepository.createUser(username, email, birthDay, departmentId);
    }
}

package com.vti.backend.service;

import com.vti.backend.repository.DepartmentRepository;
import com.vti.entity.Department;

import java.util.List;

public class DepartmentService implements IDepartmentService{
    DepartmentRepository departmentRepository = new DepartmentRepository();
    @Override
    public List<Department> getDepartments() {
        return departmentRepository.getDepartments();
    }

    @Override
    public boolean isDepartmentNameExists(String name) {
        return departmentRepository.isDepartmentNameExists(name);
    }

    @Override
    public void createDepartment(String name) {
        departmentRepository.createDepartment(name);
    }
}

package com.vti.backend.repository;

import com.vti.entity.Account;
import com.vti.entity.Department;
import com.vti.entity.Role;
import com.vti.entity.User;
import com.vti.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    public User login(String username, String password){
        try {
            String sql = "SELECT * FROM assignment10.user WHERE username = ? and password = ?";

            PreparedStatement preparedStatement = JdbcUtils.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                User user = new User();
                user.setUsername(username);
                user.setEmail(resultSet.getString("email"));
                String roleString = resultSet.getString("role"); //ADMIN || USER
                user.setRole(Role.valueOf(roleString));
                return user;
            }
        } catch (Exception e){
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return null;
    }

    public List<User> getAllUsers(){
        List<User> userList = new ArrayList<>();
        String sql = "SELECT * FROM assignment10.user \n" +
                " LEFT JOIN Department D on D.department_id = User.department_id";
        Connection connection = JdbcUtils.getConnection();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                // Lấy giá trị từng hàng gán vào đối tượng account tương ứng
                User user = new User();
                Department department = new Department();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("username"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));

                department.setDepartmentName(resultSet.getString("department_name"));
                user.setDepartment(department);
                userList.add(user);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return userList;
    }

    public User findUserById(int id){
        String sql = "SELECT * FROM assignment10.user U" +
                " LEFT JOIN Department D on D.department_id = U.department_id WHERE U.id = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                User user = new User();
                Department department = new Department();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("username"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));
                department.setDepartmentName(resultSet.getString("department_name"));
                user.setDepartment(department);
                return user;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return null;
    }

    public List<User> findUser(String key){
        List<User> userList = new ArrayList<>();
        key = "%" + key + "%";
        String sql = "SELECT * FROM assignment10.user U" +
                " LEFT JOIN Department D on D.department_id = U.department_id WHERE U.username like ? OR U.email like ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, key);
            preparedStatement.setString(2, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                // Lấy giá trị từng hàng gán vào đối tượng account tương ứng
                User user = new User();
                Department department = new Department();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("username"));
                user.setEmail(resultSet.getString("email"));
                user.setDateOfBirth(resultSet.getString("date_of_birth"));

                department.setDepartmentName(resultSet.getString("department_name"));
                user.setDepartment(department);
                userList.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JdbcUtils.closeConnection();
        }
        return userList;
    }

    public void deleteUserById(int id){
        String sql = "DELETE FROM assignment10.user U WHERE U.id = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            int rs = preparedStatement.executeUpdate();
            if (rs == 0){
                System.err.println("Xoá user thất bại!");
            } else {
                System.out.println("Đã xoá " + rs + " bản ghi");
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
    }

    public void updateUser(int id, String oldPassword, String newPassword) {
        String sql = "UPDATE assignment10.user Set password = ? WHERE id = ? and password = ?";
        try {
            Connection connection = JdbcUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // CallableStatement callableStatement: Dùng tiền cho việc : call 1 store procedure trong my sql
            preparedStatement.setString(1, newPassword);
            preparedStatement.setInt(2, id);
            preparedStatement.setString(3, oldPassword);

            int rs = preparedStatement.executeUpdate();
            if (rs == 0) {
                System.err.println("Thay đổi password thất bại!");
            } else {
                System.out.println("Thay đổi password thành công!");
            }
            JdbcUtils.closeConnection();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void createUser(String username, String email, String birthDay, int departmentId){
        String sql = "INSERT INTO `assignment10`.`User` (`role`, `username`, `password`, `email`, `date_of_birth`, department_id) " +
                "VALUES ('USER', ?, '123456', ?, ?, ?);";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, birthDay);
            preparedStatement.setInt(4, departmentId);

            int rs = preparedStatement.executeUpdate();
            if (rs == 0) {
                System.err.println("Thêm mới không thành công");
            } else {
                System.out.println("Đã thêm mới được " + rs + " User");
            }
            JdbcUtils.closeConnection();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

}

package com.vti.backend.repository;

import com.vti.entity.Department;
import com.vti.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository {
    public List<Department> getDepartments(){
        String sql = "SELECT * FROM department";
        Connection connection = JdbcUtils.getConnection();
        List<Department> departmentList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Department department = new Department();
                department.setDepartmentId(resultSet.getInt("department_Id"));
                department.setDepartmentName(resultSet.getString("department_name"));

                departmentList.add(department);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return departmentList;
    }

    public boolean isDepartmentNameExists(String name){
//        String sql = "SELECT * FROM department where department_name = ?";
        String sql = "CALL checkDepartmentName(?);";
        Connection connection = JdbcUtils.getConnection();

        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(sql);
//            preparedStatement.setString(1, name);
//            ResultSet resultSet = preparedStatement.executeQuery();
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, name);
            ResultSet resultSet = callableStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JdbcUtils.closeConnection();
        }
    }

    public void createDepartment(String name){
        Connection connection = JdbcUtils.getConnection();
        try {
            String sql = "insert into jdbc.department(departmentName) values ( ? )";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,name);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet!=0){
                System.out.println("Thêm mới thành công");
            } else {
                System.out.println("Thêm mới thất bại");
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
}

package com.vti.backend.repository;

import com.vti.entity.Account;
import com.vti.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountRepository {
    public void createAccount(String username, String email, String password) {
        // Tạo 1 câu query -> tương ứng với chức năng muốn sử dụng
        String sql = "INSERT INTO jdbc.Account (full_name, email, password) VALUES(?, ?, ?)";
        // Kết nối tới DB -> tạo 1 phiên làm việc
        Connection connection = JdbcUtils.getConnection();
        try {
            // Tạo statement tương ứng với câu query ( có biến truyền vào: PreparedStatement
            // không có biến truyền vào: Statement)
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);
            // Execute câu query -> lấy kết quả (result)
            int resultSet = preparedStatement.executeUpdate();
            // kiểm tra sự thành công và thông báo
            if (resultSet == 0) {
                System.err.println("Thêm mới thất bại!");
            } else {
                System.out.println("Thêm mới thành công!");
            }
            JdbcUtils.closeConnection();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public List<Account> getAllAccount() {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM jdbc.Account";
        // Kết nối tới DB -> tạo 1 phiên làm việc
        Connection connection = JdbcUtils.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                // Lấy giá trị từng hàng gán vào đối tượng account tương ứng
                Account account = new Account();
                int accountID = resultSet.getInt("account_id");
                account.setAccountId(accountID);

                account.setFullName(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassword(resultSet.getString("password"));
                accounts.add(account);
            }

            JdbcUtils.closeConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return accounts;
    }

    public void updateAccount(int id, String oldPassword, String newPassword) {
        String sql = "UPDATE jdbc.Account Set password = ? WHERE account_id = ? and password = ?";
        try {
            Connection connection = JdbcUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            // CallableStatement callableStatement: Dùng tiền cho việc : call 1 store procedure trong my sql
            preparedStatement.setString(1, newPassword);
            preparedStatement.setInt(2, id);
            preparedStatement.setString(3, oldPassword);

            int rs = preparedStatement.executeUpdate();
            if (rs == 0) {
                System.err.println("Thay đổi password thất bại!");
            } else {
                System.out.println("Thay đổi password thành công!");
            }
            JdbcUtils.closeConnection();


        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void deleteAccount(int id) {
        String sql = "DELETE FROM jdbc.Account Where account_id = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            int rs = preparedStatement.executeUpdate();
            if (rs == 0) {
                System.err.println("Xoá thất bại!");
            } else {
                System.out.println("Xoá thành công!");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
    }

    public List<Account> findAllByEmail(String key) {
        String sql = "SELECT * FROM jdbc.Account where email like ?";
        Connection connection = JdbcUtils.getConnection();
        String words = "%" + key + "%";
        List<Account> accountList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, words);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Account account = new Account();
                int accountID = resultSet.getInt("account_id");
                account.setAccountId(accountID);
                account.setFullName(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassword(resultSet.getString("password"));
                accountList.add(account);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return accountList;
    }

    public boolean login(String email, String password){
        try {
            String sql = "SELECT * FROM jdbc.Account WHERE email = ? and password = ?";

            PreparedStatement preparedStatement = JdbcUtils.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        } finally {
            JdbcUtils.closeConnection();
        }
    }
}

package demo;

public class ABC {
    public static void main(String[] args) {
        String a = "123";
        String b = "123";
        String c = new String("123");
        String d = new String("123");

//        String ab = new StringBuilder(a).reverse().toString();
//        System.out.println(ab);
        System.out.println(a == b); //true
        System.out.println(a == c); // false

        System.out.println(a.equals(b)); //true
        System.out.println(a.equals(c)); //true

        System.out.println(c == d); // false
        System.out.println(c.equals(d)); //true
        int ab = -3;
        if (ab % 2 == 0){
            System.out.println("Đây là số lẻ");
        }

    }
}

package demo;

public class DemoBoxingUnBoxing {
    public static void main(String[] args) {
        int a = 100;
        Integer aObject = Integer.valueOf(a); //boxing: Chuyển đổi số int a thành dạng object


        Integer b = new Integer(5);
        int bPrimitive = b.intValue(); // unboxing: Chuyển đổi từ kiểu dữ liệu Object -> primitive


    }
}

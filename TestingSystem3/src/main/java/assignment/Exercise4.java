package assignment;

import java.util.Arrays;
import java.util.Scanner;

public class Exercise4 {
    Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Exercise4 exercise4 = new Exercise4();
        exercise4.question13();
    }

    public void question1(){
        System.out.println("Mời bạn nhập vào 1 chuỗi: ");
        String input = scanner.nextLine();
        int length = input.length();
        System.out.println("Số ký tự là: " + length);

    }

    public void question2(){
        System.out.println("Mời bạn nhập vào chuỗi 1: ");
        String input1 = scanner.nextLine();
        System.out.println("Mời bạn nhập vào chuỗi 2: ");
        String input2 = scanner.nextLine();

        String rs1 = input1 + input2;
        String rs2 = input1.concat(input2);

        System.out.println(rs1);
        System.out.println(rs2);
    }

    public void question3(){
// nguyen -> Nguyen
        System.out.println("Mời bạn nhập vào tên: ");
        String name = scanner.nextLine();
        String first = String.valueOf(name.charAt(0)).toUpperCase();
        String other = name.substring(1);
        System.out.println(first + other);
    }

    public void question4(){
// nguyen -> Nguyen
        System.out.println("Mời bạn nhập vào tên: ");
        String name = scanner.nextLine();

        for (int i = 0; i < name.length(); i++) {
            String text = "Ký tự thứ " + (i + 1) + " là: " + String.valueOf(name.charAt(i)).toUpperCase();
            System.out.println(text);
        }
    }

    public void question6() {
// nguyen -> Nguyen
        System.out.println("Mời bạn nhập vào tên đầy đủ: ");
        String fullName = scanner.nextLine();
        String[] arrName = fullName.split(" ");
        String ho = arrName[0];
        String midName = arrName[1];
        String name = arrName[2];

        System.out.println("Tên: " + name);
        System.out.println("Họ:: " + ho);
        System.out.println("Tên đệm: " + midName);
    }

    public void question11() {
        String a1 = "abc123abca";
        int count = 0;
        for (int i = 0; i < a1.length(); i++) {
            if (a1.charAt(i) == 'a'){
                count++;
            }
        }
        System.out.println(count);
    }

    public void question12() {
        String a1 = "I am developer";
        String[] arrString = a1.split(" ");
        String kq = "";
        for (int i = arrString.length -1 ; i >= 0 ; i--) {
            kq += " " + arrString[i];
        }
        System.out.println(kq.trim());
    }

    public void question13() {
        String s1 = "abcnaefawhfeio1aldnvadsnv";
        boolean kq = false;
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) >= 48 && s1.charAt(i) <= 57){
                kq = true;
            }
        }
        System.out.println(kq);
    }

    public void questionxx() {
        String a1 = "123";
        System.out.println(a1.replace("2", "*"));
    }

}

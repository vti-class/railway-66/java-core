package com.vti;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Exercise3 {
//    public static void main(String[] args) {
//        Exercise3 exercise3 = new Exercise3();
//        exercise3.question1();
//        exercise3.question5();
//        int[] arrInt = {1,2};
//
//        //String abc = (điều kiện) ? (Giá trị trả về nếu đk đúng) : (giá trị trả về khi đk sai)
//    }


    // Phạm vi truy cập của method: public  (x)
    // kiểu dữ liệu trả về: void
    // Tên method và khai báo biến truyền vào
    public void question1(){
        Date date = new Date();
        Locale locale = new Locale("vi", "VN");
        DateFormat dateFormat = SimpleDateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String dateString = dateFormat.format(date);
        System.out.println(dateString);
    }

    public static void question2(){
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String dateString = dateFormat.format(date);
        System.out.println(dateString);
    }

    public void question3(){
        Date date = new Date();
        String patten = "yyyy";
        DateFormat dateFormat = new SimpleDateFormat(patten);
        String dateString = dateFormat.format(date);
        System.out.println(dateString);
    }

    public void question4(){
        String patten = "MM-yyyy";
        DateFormat dateFormat = new SimpleDateFormat(patten);
        String dateString = dateFormat.format(new Date());
        System.out.println(dateString);
    }

    public void question5(){
        String patten = "MM-dd";
        DateFormat dateFormat = new SimpleDateFormat(patten);
        String dateString = dateFormat.format(new Date());
        System.out.println(dateString);
    }

}

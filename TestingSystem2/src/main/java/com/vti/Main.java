package com.vti;

public class Main {
    public static void main(String[] args) {
        //C1: Khởi tạo đối tương Ex3
        Exercise3 exercise3 = new Exercise3();
        exercise3.question1();

        // C2: Ko cần khởi tạo đối tượng Ex3 mà gọi trực tiếp thông qua từ khoá static
        Exercise3.question2();
    }
}
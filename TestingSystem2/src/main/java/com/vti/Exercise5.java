package com.vti;

import java.util.Scanner;

public class Exercise5 {
    public static void main(String[] args) {
        Exercise5 exercise5 = new Exercise5();
        exercise5.question8();
    }

    public void question1() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số thứ 1: ");
        int number1 = scanner.nextInt();
        System.out.println("Nhập số thứ 2: ");
        int number2 = scanner.nextInt();
        System.out.println("Nhập số thứ 3: ");
        int number3 = scanner.nextInt();

        System.out.println("3 số vừa nhập là: " + number1 + ", " + number2 + ", " + number3);
    }

    public void question2() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số thực thứ 1:");
        float number = scanner.nextFloat();
    }

    public void question3() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ và tên");
        String fullName = scanner.nextLine();
    }

    public void question5() {
        System.out.println("Thêm mới 1 account");
    }
    public void question6() {
        Scanner scanner = new Scanner(System.in);
        Department department = new Department();
        System.out.println("Nhập vào id của department: ");
        int departmentId = scanner.nextInt();

        System.out.println("Nhập vào tên của department: ");
        String name = scanner.nextLine();

        department.departmentId = departmentId;
        department.departmentName = name;

        System.out.println(department.departmentName);
    }

    public void question7() {
        Scanner scanner = new Scanner(System.in);

// Để tránh TH bị trôi con trỏ -> khai báo biến int như sau:
//        int number = Integer.parseInt(scanner.nextLine());
//        String string = scanner.nextLine();
//        System.out.println(string + number);
        System.out.println("Nhập vào số chẵn: ");
        int a = scanner.nextInt();
        while (true){
            if (a % 2 == 0){
                System.out.println("Số bạn vừa nhập vào là: " + a);
                break;
            }
            System.out.println("Số b vừa nhập ko phải là số chẵn, mời nhập lại: ");
            a = scanner.nextInt();
        }
    }

    public void question8() {
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("mời bạn nhập vào chức năng muốn sử dụng");
            System.out.println("1. Thêm mới 1 account");
            System.out.println("2. Thêm mới 1 department");
            System.out.println("3. Thoát");
            int number = scanner.nextInt();
            switch (number){
                case 1:
                    question5();
                    break;
                case 2:
                    question6();
                    break;
                case 3:
                    return;
                default:
                    System.out.println("Mời bạn nhập lại");
            }
        }
    }



}

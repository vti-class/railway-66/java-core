package com.vti;

import java.util.Random;

public class Exercise4 {
    public static void main(String[] args) {
        Exercise4 exercise4 = new Exercise4();
        exercise4.question1();
    }

    public void question1(){
        Random random =new Random();
        int number = random.nextInt();
        System.out.println(number);
    }

    public void question2(){
        Random random = new Random();
        float number = random.nextFloat();
        System.out.println(number);
    }

    public void question7(){
        Random random = new Random();
        //random.nextInt(n) -> giá trị random sẽ đi từ 0 -> n-1
        // mục đích: Tạo random từ 100 -> 999 + 100
        int number = random.nextInt(900) + 100;
        System.out.println(number);
    }
}

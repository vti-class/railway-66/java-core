package com.vti;

public class Exercise6 {
    public static void main(String[] args) {
        Exercise6 exercise6 = new Exercise6();
        exercise6.question5(1,5,3);
    }

    public void question1(){
        for (int i = 0; i < 10; i++) {
            if (i%2==0){
                System.out.println(i);
            }
        }
    }

    public void question1_C2(){
        for (int i = 0; i < 10; i+=2) {
                System.out.println(i);
        }
    }

    // tạo ra 1 method để truyền vào method 2 số nguyên -> in ra số trung bình cộng của 2 số đó
    public void question4(int a, int b){
        double rs = (( double)a+b)/2;
        System.out.println(rs);
    }

    // tạo ra 1 method để truyền vào 3 số nguyên, in ra theo thứ tự tăng dần
    public void question5(int a, int b, int c){
        int min = 0;
        int max = 0;
        int other = 0;

        int[] numbers = {a, b, c};
        for (int i = 0; i < numbers.length; i++) {
            if ( numbers[i] <= a && numbers[i] <= b && numbers[i] <= c){
                min = numbers[i];
            }else if (numbers[i] >= a && numbers[i] >= b && numbers[i] >= c){
                max = numbers[i];
            } else {
                other = numbers[i];
            }
        }
        System.out.println(min + " " +  other + " " + max);
    }



}

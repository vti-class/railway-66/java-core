package com.vti.testingsystem89.frontend;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class DemoSet {
    public static void main(String[] args) {
        demoHashSet();
    }

    public static void demoHashSet(){
        // Set ko lưu 2 phần tử giống nhau
        Set<Integer> integerSet = new HashSet<>();
        integerSet.add(1); // index 0
        integerSet.add(2); // index 1
        integerSet.add(4);
        integerSet.add(3);
        integerSet.add(5);
        integerSet.add(1);
        System.out.println(integerSet);

        // Kt số phần tử:
        System.out.println("Số phần tử trong set là: " + integerSet.size());

        // Không có hàm get(index) so với list
//        System.out.println("Vị trí có index = 1 là: " + integerSet.);

        // Xoá phần tử ( có 2 cách )
        // C1: Truyền index của phần tử muốn xoá ( thì dữ liệu trả về là đối tượng E mà mình làm việc )
        System.out.println("Xoá: " + integerSet.remove(1));
        System.out.println(integerSet);

        // C1: Truyền vào đối tượng mà mình muốn xoá ( Đối tượng truyền vào phải giống E) ( dữ liệu trả về là boolean )
        System.out.println("Xoá: " + integerSet.remove("2"));
        System.out.println(integerSet);

        Iterator<Integer> stringIterator = integerSet.iterator();
        while (stringIterator.hasNext()){
            System.out.println(stringIterator.next());
        }
    }
}

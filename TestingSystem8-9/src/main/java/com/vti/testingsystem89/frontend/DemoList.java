package com.vti.testingsystem89.frontend;

import java.util.*;

public class DemoList {
    public static void main(String[] args) {
        demoSort();
    }

    public static void demoArrayList(){
        List<String> integerList = new ArrayList<>();
        integerList.add("1"); // index 0
        integerList.add("2"); // index 1
        integerList.add("3");
        integerList.add("4");
        integerList.add("5");
        integerList.add("1");

        // Kt số phần tử:
        System.out.println("Số phần tử trong list là: " + integerList.size());

        // Lấy phần tử ở vị trí index thứ n ( index bắt đầu từ 0)
        System.out.println("Vị trí có index = 1 là: " + integerList.get(1));

        // Xoá phần tử ( có 2 cách )
        // C1: Truyền index của phần tử muốn xoá ( thì dữ liệu trả về là đối tượng E mà mình làm việc )
//        System.out.println("Xoá: " + integerList.remove(1));
//        System.out.println(integerList);

        // C1: Truyền vào đối tượng mà mình muốn xoá ( Đối tượng truyền vào phải giống E) ( dữ liệu trả về là boolean )
        System.out.println("Xoá: " + integerList.remove("2"));
        System.out.println(integerList);

        Iterator<String> stringIterator = integerList.iterator();
        while (stringIterator.hasNext()){
            System.out.println(stringIterator.next());
        }
    }

    public static void demoLinkList(){
        List<String> integerList = new LinkedList<>();
        integerList.add("1"); // index 0
        integerList.add("8"); // index 1
        integerList.add("10");
        integerList.add("4");
        integerList.add("5");
        integerList.add("1");

        // Kt số phần tử:
        System.out.println("Số phần tử trong list là: " + integerList.size());

        // Lấy phần tử ở vị trí index thứ n ( index bắt đầu từ 0)
        System.out.println("Vị trí có index = 1 là: " + integerList.get(1));

        // Xoá phần tử ( có 2 cách )
        // C1: Truyền index của phần tử muốn xoá ( thì dữ liệu trả về là đối tượng E mà mình làm việc )
//        System.out.println("Xoá: " + integerList.remove(1));
//        System.out.println(integerList);

        // C1: Truyền vào đối tượng mà mình muốn xoá ( Đối tượng truyền vào phải giống E) ( dữ liệu trả về là boolean )
        System.out.println("Xoá: " + integerList.remove("2"));
        System.out.println(integerList);

        Iterator<String> stringIterator = integerList.iterator();
        while (stringIterator.hasNext()){
            System.out.println(stringIterator.next());
        }
    }

    @SuppressWarnings({})
    public static void demoSort(){
        List<String> integerList = new ArrayList<>();
        integerList.add("1"); // index 0
        integerList.add("8"); // index 1
        integerList.add("3");
        integerList.add("2");
        integerList.add("5");
        integerList.add("1");
        System.out.println(integerList);

        // Sắp xếp theo thứ tự tăng dần
//        Collections.sort(integerList);
//        System.out.println(integerList);

        // Đảo ngược lại danh sách
        Collections.reverse(integerList);
        System.out.println(integerList);

        List<String> copy = new LinkedList<>();
        copy = integerList;
        copy.addAll(integerList);


    }

}
